<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
use Carbon\Carbon;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'first_name' => 'required|string|max:255',
            'last_name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {

        // init variables

        $birthday   = null;
        $street     = null;
        $city       = null;
        $state      = null;
        $country    = null;
        $zip        = null;
        $lat        = null;
        $lng        = null;

        if(isset($data['latitude']) && isset($data['longitude'])) {
            $lat = $data['latitude'];
            $lng = $data['longitude'];
        } else {
            $client = new Client();

            $api = "https://maps.googleapis.com/maps/api/geocode/json?address=";

            $address = $data['address'];

            $call = $api . $address;

            $response = $client->get($call);

            if($response->getStatusCode() == 200) {
                $payload = json_decode($response->getBody());

                // process the results
                if($payload->results) {
                    $payload = $payload->results;
                    if($payload[0]) {
                        $payload = $payload[0];
                    }
                }

                // get the lat and lng
                if($payload->geometry) {
                    $geometery = $payload->geometry;
                    $location = $geometery->location;
                    if($location->lat && $location->lng) {
                        $lat = $location->lat;
                        $lng = $location->lng;
                    }
                }

                // get the street, city, state, zip
                if($payload->address_components) {
                    $address = $payload->address_components;

                    foreach($address as $component) {

                        if (in_array("street_number", $component->types)) {
                            $street = $component->long_name;
                        }

                        if (in_array("route", $component->types)) {
                            $street .= ' ' . $component->long_name;
                        }

                        if (in_array("locality", $component->types)) {
                            $city = $component->long_name;
                        }

                        if (in_array("administrative_area_level_1", $component->types)) {
                            $state = $component->long_name;
                        }

                        if (in_array("country", $component->types)) {
                            $country = $component->long_name;
                        }

                        if (in_array("postal_code", $component->types)) {
                            $zip = $component->long_name;
                        }

                    }
                }
            }
        }

        if($data['birthday']) {
            $birthday = new Carbon($data['birthday']);
        }

        return User::create([
            'first_name' => $data['first_name'],
            'last_name' => $data['last_name'],
            'email' => $data['email'],
            'street' => $street,
            'city' => $city,
            'state' => $state,
            'country' => $country,
            'zip' => $zip,
            'phone' => $data['phone'],
            'birthday' => $birthday,
            'latitude' => $lat,
            'longitude' => $lng,
            'password' => bcrypt($data['password']),
        ]);
    }
}
