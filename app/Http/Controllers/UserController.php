<?php

namespace App\Http\Controllers;

use App\User;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class UserController extends Controller
{
    public function home () {
        if(!Auth::check()) {
            return view('welcome');
        }

        return view('dashboard');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return User::get();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        User::create($request->all());
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        //return User::findOrFail($user->id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        //
    }

    /**
     * Get nearby users.
     *
     * @param  $max_distance
     * @return \Illuminate\Http\Response
     */
    public function nearBy($max_distance = 20)
    {

        if(!Auth::check()) {
            return "You are not logged in.";
        }

        $circle_radius = 3959;

        $user = Auth::user();
        $lat = $user->latitude;
        $lng = $user->longitude;

        if(!$lat || !$lng) {
            return [];
        }

        return DB::select('SELECT * FROM
                        (SELECT id, first_name, last_name, street, city, state, country, zip, phone, email, latitude, longitude, (' . $circle_radius . ' * acos(cos(radians(' . $lat . ')) * cos(radians(latitude)) *
                        cos(radians(longitude) - radians(' . $lng . ')) +
                        sin(radians(' . $lat . ')) * sin(radians(latitude))))
                        AS distance
                        FROM users) AS distance
                    WHERE distance < ' . $max_distance . '
                    ORDER BY distance
                    LIMIT 50');
    }

}
