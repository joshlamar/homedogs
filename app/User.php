<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

use Auth;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name', 'last_name', 'birthday', 'email', 'password', 'street', 'city', 'state', 'country', 'zip', 'phone', 'latitude', 'longitude'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];


    /**
     * Get the distance in miles a user is away from auth user.
     *
     * @return integer
     */
    public function getMilesAwayAttribute()
    {

        if(!Auth::check()) {
            return 0;
        }

        $earthRadius = 3959;

        $user = Auth::user();

        $lat1 = $user->latitude;
        $lat2 = $user->longitude;

        $lng1 = $this->latitude;
        $lng2 = $this->longitude;

        // convert from degrees to radians
        $latFrom = deg2rad($lat1);
        $lonFrom = deg2rad($lat2);
        $latTo = deg2rad($lng1);
        $lonTo = deg2rad($lng2);

        $latDelta = $latTo - $latFrom;
        $lonDelta = $lonTo - $lonFrom;

        $angle = 2 * asin(sqrt(pow(sin($latDelta / 2), 2) +
                cos($latFrom) * cos($latTo) * pow(sin($lonDelta / 2), 2)));
        return round($angle * $earthRadius);
    }

}
